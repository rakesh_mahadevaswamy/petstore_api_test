package com.petstore.utils;

import com.petstore.model.Category;
import com.petstore.model.ServiceDetails;
import com.petstore.model.Tag;
import net.serenitybdd.rest.SerenityRest;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;

public class Utils {

    public static ServiceDetails petRequest(Integer tagId, String tagName, Integer catId, String catName, Integer petId, String petName, String status) {
        ServiceDetails serviceDetails = new ServiceDetails();
        Category categoryInfo = new Category();
        Tag tagOne = new Tag();
        tagOne.setId(tagId);
        tagOne.setName(tagName);
        categoryInfo.setId(catId);
        categoryInfo.setName(catName);
        serviceDetails.setid(petId);
        serviceDetails.setName(petName);
        String[] photo = {"testPhotp", "test1"};
        serviceDetails.setPhotoUrls(photo);
        serviceDetails.setStatus(status);
        serviceDetails.setCategory(categoryInfo);
        ArrayList<Tag> tags = new ArrayList<Tag>();
        tags.add(tagOne);
        Tag[] tagArray = new Tag[tags.size()];
        tags.toArray(tagArray);
        serviceDetails.setTags(tagArray);
        return serviceDetails;
    }

    public static void verifyPetDelete(Integer petId) {
                SerenityRest.given()
                .pathParam("petId",petId)
                .get("pet" + "/{petId}")
                .then()
                .body(containsString("Pet not found"));
    }

    public static void verifyFetchedPetResponse(Integer petId) {
        SerenityRest.given()
                .pathParam("petId",petId)
                .get("pet" + "/{petId}")
                .then()
                .body("id",equalTo(petId));
    }
}
