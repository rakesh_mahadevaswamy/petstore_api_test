package com.petstore.runner;

import com.petstore.petDetails.PetResponse;
import com.petstore.utils.Constant;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.jruby.RubyProcess;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Map;

import static com.petstore.utils.Constant.petId;
import static com.petstore.utils.Utils.*;
import static org.hamcrest.Matchers.equalTo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SerenityRunner.class)
public class PetSerenityTest {

    @Steps
    PetResponse petResponse;

    @BeforeClass
    public static void init(){
        RestAssured.baseURI= Constant.baseURI;
    }



    @Title("Create a pet")
    @Test
    public void test01(){
        Integer tagId=55;
        String tagName="Tag test";
        Integer petIdDetails=99;
        String petName="DOG";
        Integer categoryId=88;
        String categoryName="Gun dog";
        String status="available";
        Object sd=petRequest(tagId,tagName,categoryId,categoryName,petIdDetails,petName,status);
        SerenityRest.given()
                .when()
                .log()
                .all()
                .contentType(ContentType.JSON)
                .body(sd)
                .post("pet")
                .then()
                .log()
                .all()
             .body("id", equalTo(99))
                .body("name", equalTo("DOG"));
        Map<String, String> actualResponse = petResponse.returned();
        String petIdInfo = actualResponse.get("id");
         petId=(int) Math.round(Float.parseFloat(petIdInfo));
    }

    @Title("Pet can be fetched by Id")
    @Test
    public void test02(){
//        Integer petId=99;
        SerenityRest.given()
                .when()
                .get("pet/"+petId)
                .then()
                .log()
                .all()
                .statusCode(200)
                .body("id", equalTo(petId));
    }

    @Title("Update a pet by using id")
    @Test
    public void test03(){
        Integer tagId=55;
        String tagName="Tag test";
        Integer petIdDetails=99;
        String petName="DOGGIE";
        Integer categoryId=88;
        String categoryName="Toy Dog";
        String status="available";
        Object sd=petRequest(tagId,tagName,categoryId,categoryName,petIdDetails,petName,status);
        SerenityRest.given()
                .when()
                .log()
                .all()
                .contentType(ContentType.JSON)
                .body(sd)
                .put("pet")
                .then()
                .log()
                .all()
                .statusCode(200);
        verifyFetchedPetResponse(Constant.petId);
    }

    @Title("Delete a pet by using id")
    @Test
    public void test04(){
//        Integer petId=99;
        SerenityRest.given()
                .when()
                .delete("pet/"+petId)
                .then()
                .log()
                .all()
                .statusCode(200)
                .body("message", equalTo(String.valueOf(petId)));
                 verifyPetDelete(petId);
    }

}
