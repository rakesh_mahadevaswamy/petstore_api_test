package com.petstore.runner;

import com.petstore.utils.Constant;
import io.restassured.RestAssured;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Title;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.equalTo;

@RunWith(SerenityRunner.class)
public class UserSerenityTest {

    @BeforeClass
    public static void init(){
        RestAssured.baseURI= Constant.baseURI;
    }

    @Title("Verify the error response if non-registered user tried to search by name")
    @Test
    public void verifyErrorResponseForNoUser(){
        SerenityRest.given()
                .when()
                .get("user/Test1234")
                .then()
                .log()
                .all()
                .statusCode(404)
                .body("message", equalTo("User not found"));

    }
}
