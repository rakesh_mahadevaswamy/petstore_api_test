package com.petstore.runner;

import com.petstore.utils.Constant;
import io.restassured.RestAssured;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Title;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.equalTo;


@RunWith(SerenityRunner.class)
public class OrderSerenityTest {

    @BeforeClass
    public static void init(){
        RestAssured.baseURI= Constant.baseURI;
    }

    @Title("Verify the error message is provided when searching for an order that does not exist")
    @Test
    public void verifyErrorMessageForNoOder(){
        SerenityRest.given()
                .when()
                .get("/store/order/9")
                .then()
                .log()
                .all()
                .statusCode(404)
                .body("message", equalTo("Order not found"));
    }
}
