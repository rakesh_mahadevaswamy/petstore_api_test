package com.petstore.runner;

import com.petstore.cucumber.steps.UserSerenitySteps;
import com.petstore.utils.Constant;
import io.restassured.RestAssured;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.equalTo;

@UseTestDataFrom("testdata/users.csv")
@RunWith(SerenityParameterizedRunner.class)
public class MultiUsersSerenityTest {

    private Integer id;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    private String password;
    private Integer phone;
    private Integer userStatus;

    @BeforeClass
    public static void init(){
        RestAssured.baseURI= Constant.baseURI;
    }

    @Steps
    UserSerenitySteps steps;

    @Title("Multiple Users are created using external file")
    @Test
    public void usersCreatedUsingExternalFile(){
        steps.createUser(id,userName,firstName,lastName,email,password,phone,userStatus)
                .statusCode(200)
                .body("message", equalTo(id.toString()));
    }
}
