package com.petstore.cucumber.steps;

import com.petstore.model.User;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class UserSerenitySteps {
    @Step
    public ValidatableResponse createUser(Integer id, String userName, String firstName, String lastName, String email, String password, Integer phone, Integer userStatus){
        User user = new User();
        user.setId(id);
        user.setUserName(userName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setUserStatus(userStatus);
        return SerenityRest.given()
                .when()
                .log()
                .all()
                .contentType(ContentType.JSON)
                .body(user)
                .post("user")
                .then()
                .log()
                .all();
    }
}
