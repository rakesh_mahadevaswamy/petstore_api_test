# PetStore-api-test

Welcome to PetStore REST testing where we can learn to fetch orders, user creations, and pet CRUD operations.

## Framework details
Serenity BDD + REST with GitLab CI integration framework using pet store APIs.
* [REST-Assured](http://rest-assured.io/) library for REST service testing
* [Serenity-BDD](https://thucydides.info/#/) testing framework
* [GitLab-runner](https://docs.gitlab.com/runner/) for CI platform
* Gherkin style approach test
* POJO concepts are used for validations

##### API Documents :
https://petstore.swagger.io

## Features
* Creation of multiple users using external file (DataDriven) 
* Fetch order details using store order id
* Perform CRUD operations for pet

## Installation
* JRE
* Maven

## Executing Tests
**Maven Commands:**
- Run the project locally by using  - `mvn clean verify serenity:aggregate`

## Serenity HTML Reports
Reports will be generated locally in `target/site/serenity/index.html`
